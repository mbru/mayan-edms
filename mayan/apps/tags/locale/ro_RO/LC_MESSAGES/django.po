# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Abalaru Paul <paul.abalaru@hotmail.com>, 2013
# Badea Gabriel <gcbadea@gmail.com>, 2013
# Stefaniu Criste <gupi@hangar.ro>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-10-29 12:45-0400\n"
"PO-Revision-Date: 2018-09-27 02:31+0000\n"
"Last-Translator: Roberto Rosario\n"
"Language-Team: Romanian (Romania) (http://www.transifex.com/rosarior/mayan-edms/language/ro_RO/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ro_RO\n"
"Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?2:1));\n"

#: apps.py:46 apps.py:108 apps.py:115 apps.py:136 apps.py:138 events.py:7
#: forms.py:32 links.py:45 menus.py:15 models.py:38 permissions.py:7
#: views.py:212 workflow_actions.py:19 workflow_actions.py:64
msgid "Tags"
msgstr "Etichete"

#: apps.py:129 models.py:32
msgid "Documents"
msgstr "Documente"

#: events.py:10
msgid "Tag attached to document"
msgstr ""

#: events.py:13
msgid "Tag created"
msgstr ""

#: events.py:16
msgid "Tag edited"
msgstr ""

#: events.py:19
msgid "Tag removed from document"
msgstr ""

#: links.py:17 workflow_actions.py:71
msgid "Remove tag"
msgstr "Elimină eticheta"

#: links.py:20 links.py:28
msgid "Attach tags"
msgstr ""

#: links.py:24
msgid "Remove tags"
msgstr "Elimină etichete"

#: links.py:33
msgid "Create new tag"
msgstr ""

#: links.py:37 links.py:55 views.py:148
msgid "Delete"
msgstr "Șterge"

#: links.py:40
msgid "Edit"
msgstr "Modifică"

#: links.py:52
msgid "All"
msgstr ""

#: models.py:24
msgid "A short text used as the tag name."
msgstr ""

#: models.py:25 search.py:16
msgid "Label"
msgstr "Etichetă"

#: models.py:28
msgid "The RGB color values for the tag."
msgstr ""

#: models.py:29 search.py:20
msgid "Color"
msgstr "Culoare"

#: models.py:37
msgid "Tag"
msgstr "Etichetă"

#: models.py:61
msgid "Preview"
msgstr ""

#: models.py:86
msgid "Document tag"
msgstr ""

#: models.py:87
msgid "Document tags"
msgstr ""

#: permissions.py:10
msgid "Create new tags"
msgstr "Crează etichetă nouă"

#: permissions.py:13
msgid "Delete tags"
msgstr "Ștergeți etichetele"

#: permissions.py:16
msgid "View tags"
msgstr "Vezi etichetele"

#: permissions.py:19
msgid "Edit tags"
msgstr "Editați etichetele"

#: permissions.py:22
msgid "Attach tags to documents"
msgstr "Atașați etichete la documente"

#: permissions.py:25
msgid "Remove tags from documents"
msgstr "Îndepărtați etichetele de pe documente"

#: serializers.py:39
msgid ""
"Comma separated list of document primary keys to which this tag will be "
"attached."
msgstr ""

#: serializers.py:86
msgid ""
"API URL pointing to a tag in relation to the document attached to it. This "
"URL is different than the canonical tag URL."
msgstr ""

#: serializers.py:106
msgid "Primary key of the tag to be added."
msgstr ""

#: views.py:38
#, python-format
msgid "Tag attach request performed on %(count)d document"
msgstr ""

#: views.py:40
#, python-format
msgid "Tag attach request performed on %(count)d documents"
msgstr ""

#: views.py:47
msgid "Attach"
msgstr ""

#: views.py:49
#, python-format
msgid "Attach tags to %(count)d document"
msgid_plural "Attach tags to %(count)d documents"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""

#: views.py:61
#, python-format
msgid "Attach tags to document: %s"
msgstr ""

#: views.py:70 wizard_steps.py:28
msgid "Tags to be attached."
msgstr ""

#: views.py:103
#, python-format
msgid "Document \"%(document)s\" is already tagged as \"%(tag)s\""
msgstr "Documentul \"%(document)s\" este deja etichetat cu \"%(tag)s\""

#: views.py:114
#, python-format
msgid "Tag \"%(tag)s\" attached successfully to document \"%(document)s\"."
msgstr "Eticheta \"%(tag)s\" a fost atașată cu succes la documentul \"%(document)s\"."

#: views.py:123
msgid "Create tag"
msgstr ""

#: views.py:137
#, python-format
msgid "Tag delete request performed on %(count)d tag"
msgstr ""

#: views.py:139
#, python-format
msgid "Tag delete request performed on %(count)d tags"
msgstr ""

#: views.py:146
msgid "Will be removed from all documents."
msgstr "Va fi eliminată din toate documentele."

#: views.py:150
msgid "Delete the selected tag?"
msgid_plural "Delete the selected tags?"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""

#: views.py:160
#, python-format
msgid "Delete tag: %s"
msgstr ""

#: views.py:170
#, python-format
msgid "Tag \"%s\" deleted successfully."
msgstr "Eticheta \"%s\" a fost ștersă cu succes."

#: views.py:174
#, python-format
msgid "Error deleting tag \"%(tag)s\": %(error)s"
msgstr "Eroare la ștergerea etichetă \"%(tag)s\": %(error)s"

#: views.py:189
#, python-format
msgid "Edit tag: %s"
msgstr "Modifică eticheta: %s"

#: views.py:208
msgid ""
"Tags are color coded properties that can be attached or removed from "
"documents."
msgstr ""

#: views.py:211
msgid "No tags available"
msgstr ""

#: views.py:235
#, python-format
msgid "Documents with the tag: %s"
msgstr "Documente cu eticheta: %s"

#: views.py:259
msgid "Document has no tags attached"
msgstr ""

#: views.py:266
#, python-format
msgid "Tags for document: %s"
msgstr "Etichetele documentului: %s"

#: views.py:279
#, python-format
msgid "Tag remove request performed on %(count)d document"
msgstr ""

#: views.py:281
#, python-format
msgid "Tag remove request performed on %(count)d documents"
msgstr ""

#: views.py:289
msgid "Remove"
msgstr "Şterge"

#: views.py:291
#, python-format
msgid "Remove tags to %(count)d document"
msgid_plural "Remove tags to %(count)d documents"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""

#: views.py:303
#, python-format
msgid "Remove tags from document: %s"
msgstr ""

#: views.py:312
msgid "Tags to be removed."
msgstr ""

#: views.py:345
#, python-format
msgid "Document \"%(document)s\" wasn't tagged as \"%(tag)s"
msgstr ""

#: views.py:355
#, python-format
msgid "Tag \"%(tag)s\" removed successfully from document \"%(document)s\"."
msgstr "Eticheta \"%(tag)s\" a fost eliminată cu succes din documentul \"%(document)s\"."

#: wizard_steps.py:16
msgid "Select tags"
msgstr ""

#: workflow_actions.py:21
msgid "Tags to attach to the document"
msgstr ""

#: workflow_actions.py:26
msgid "Attach tag"
msgstr "Atașează etichetă"

#: workflow_actions.py:66
msgid "Tags to remove from the document"
msgstr ""
